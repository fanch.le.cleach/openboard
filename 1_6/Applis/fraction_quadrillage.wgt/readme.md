# Fractions & Quadrillage
Application pour Opens-Sankoré / Openboard

Représenter une fraction de l'unité sur un quadrillage.
# Auteur
François Le Cléac'h https://openedu.fr
Avril 2024
# Licence 
CC BY-NC-SA
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions

Cette licence vous permet de remixer, arranger, et adapter l'œuvre à des fins non commerciales tant que vous créditer les auteurs en citant leur nom et que les nouvelles œuvres soient diffusées selon les mêmes conditions. 


# Versions
## 0.1
affichage d'un quadrillage 
## 0.2
coloration d'une case au clic
## 0.3
ajout de lignes et de colonnes
## 1.0
Sauvegarde des couleurs des cases pour éviter 
de repartir sur une grille vierge quand on modifie le nombre
de lignes ou de colonnes
## 1.1
Le clic sur une case ayant déjà la couleur de la palette l'efface.
## 1.2
Bouton pour effacer le dessin
## 1.3
Change toutes les cases ayant la couleur de la fraction lors du changement de couleur.
## 1.4
Ajout d'une palette pour les couleurs des lignes.
## 1.5
Simplification du code
## 1.6
Affichage des valeurs des fractions (étiquettes)
## 2.0
Sauvegarde/Restauration des paramètres du widget permettant de ne pas revenir à zéro lors de navigation entre les pages ou lors de la copie du widget.
## 3.0
* Réécriture intégrale pour une compatibilité **OpenBoard 1.5**
* Ajout de paramètres
    * Taille des polices utilisées
    * épaisseur des ligne
    * hauteur de la bande
    * couleurs des nombres, lignes, fond...
    * mode sombre (noir&blanc) pour un affichage sur tableau noir.
* Internationalisation de l'application (**scripts/langues.js**)
    * Français
    * Allemand
    * Anglais
* Normalisation du fichier **config.xml**
    * identifiant unique de l'application pour les mises à jours ultérieures
## 3.1
adaptation pour un fonctionnement Hors Openboard (navigateur simple)
# Sources
## Icones :
* Solar Linear Icons https://www.svgrepo.com/
## Bibliothèques :
* Raphael JS https://dmitrybaranovskiy.github.io/raphael/
* Laktek colorPicker https://github.com/laktek/really-simple-color-picker
* Jquery 3.7.1 https://jquery.com/

