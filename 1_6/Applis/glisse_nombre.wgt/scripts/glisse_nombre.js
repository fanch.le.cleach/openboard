const openboard=Boolean(window.sankore),bouton_plus = $('.plus'), bouton_moins = $('.moins'),vrai=Boolean('true'),faux=Boolean('');
var 	langue_defaut="fr",syslang=langue_defaut,
	origine=14, /* index de la cellule des unités au départ*/
	nombre=0,
	nb_chiffres=bouton_plus.length, /* Nombre de cases de la bande*/
	saut=66,// Largeur d'une case du tableau pour le glissement
	courant=origine,//---- Position de départ  
	separateur=",",code_separateur=44, // Séparateur décimal
	msg_erreur_limite="Limite du Glisse Nombre atteinte. Essayez un facteur plus petit"; // Message d'erreur
// ##### INITIALISATION
function init(){
	//----------------------------------------------------------------------------
	//---- Fonctionnalités des boutons + et - sur les chiffres du glisse_nombre 
	//----------------------------------------------------------------------------
	//---- Bouton + 
	for(var i=0;i<bouton_plus.length;i++){
		//---- Externalisation de l'ajout en raison d'un bug d'indexage 
		//---- Les fonctions ne seraient pas ajoutées au bon bouton 
		ajoute_fonction_plus(bouton_plus[i],i);
	}
	//---- Bouton - 
	for(var j=0;j<bouton_moins.length;j++){
		//---- Externalisation de l'ajout en raison d'un bug d'indexage 
		//---- Les fonctions ne seraient pas ajoutées au bon bouton 
		ajoute_fonction_moins(bouton_moins[j],j);
	}
	if (openboard){ // Si on est dans Openboard
		// Quand on revient sur le widget, on récupère les paramètres stockés
		if (window.sankore.preference('nombre')) {// Récupération des paramètres sauvegardés s'ils existent (retour sur la page)
			nombre=parseFloat(window.sankore.preference('nombre'));
			if (window.sankore.preference('Mode sombre')==='true') {$('body').addClass('mode_sombre')};//Fond noir
		}
		//Window Widget
		if (window.widget) {
		// Quand on quitte le widget
		window.widget.onleave = function(){	
			//Sauvegarde des paramètres au format 'chaine de caractères'
			window.sankore.setPreference('nombre', nombre);
			window.sankore.setPreference('Mode sombre', $('body').hasClass('mode_sombre'));
		}
	}
}
	// Initialisation des langues
	init_lang();
	// Affichage du nombre dans la zone en haut à gauche
	$("#nombre input").val(nombre.toString().replace(".",separateur)); // Remplacement du point par le séparateur décimal en fonction de la langue utilisée.
	// Encadré rouge des unités
	$("#chiffre_unite").css({
		height:($(".chiffre").outerHeight()+10)+"px",
		top:$("#ligne1").outerHeight()+"px"
		});
	// Gestion des touches dans la zone de saisie du nombre
	$("#nombre input").keypress(function(e){
	if (e.which==13) {
		let contenu=$(this).val().replace(",","."); // Si le séparateur décimal est la virgule, on la remplace par un point
		nombre=parseFloat(contenu); // Conversion de la chaîne en nombre
		place_nombre(nombre); // Placement du nombre dans le tableau
	}
	if( 47 < e.which && e.which< 58 || e.which==code_separateur) {// Cractères autorisés (Chiffres et séparateur décimal)
	}else{// sinon il ne se asse rien
		e.preventDefault();
		return false;}
	});
	// Mise à jour du texte de l'aide
	// Le titre
	$('#infos').attr('title',sankoreLang[syslang].Infos);
	// Le contenu
	$('#infos').html(sankoreLang[syslang].Txt_infos);
	// Affichage de la virgule
	$('#la_virgule').html(sankoreLang[syslang].Separateur_decimal);//  la virgule en Français, le point en Anglais... 
	// Écriture du nombre dans le glisse nombre
	place_nombre(nombre);
}
// ##### LANGUES
function init_lang(){
	//Détection de la langue
	try{
		syslang = sankore.locale().substr(0,2);
        } catch(e){
		syslang = langue_defaut;
	}
	// Chargement du fichier de langue
	sankoreLang[syslang].search;
	 // Traduction de l'interface
	$('#milliards').text(sankoreLang[syslang].milliards);
	$('#millions').text(sankoreLang[syslang].millions);
	$('#mille').text(sankoreLang[syslang].mille);
	$('#unites').text(sankoreLang[syslang].unites);
	$('#dixiemes').text(sankoreLang[syslang].dixiemes);
	$('#centiemes').text(sankoreLang[syslang].centiemes);
	$('#milliemes').text(sankoreLang[syslang].milliemes);
	$('#dixmilliemes').text(sankoreLang[syslang].dixmilliemes);
	$('.c').text(sankoreLang[syslang].c);
	$('.d').text(sankoreLang[syslang].d);
	$('.u').text(sankoreLang[syslang].u);
	separateur_decimal=sankoreLang[syslang].Separateur_decimal; //  la virgule en Français, le point en Anglais... 
	if (syslang=="en"||syslang=="de"){$("#mille").css('font-size','35px')} // Adaptation du texte trop long
	}
// ##### Place le nombre sur le glisse nombre
function place_nombre(lenombre){
	vide_bande(); //---- On vide toutes les cases avant de placer les chiffres
	var chaine=lenombre.toString(),cpt=chaine.length,partie=chaine.split("."),cpt1=partie[0].length;
	/*Partie entière*/
	for (var i=0; i<cpt1;i++) {
		$('.chiffre input:eq(' + (1-cpt1+i+courant) + ')').val(partie[0].charAt(i));}
	/*Partie décimale*/
	if(partie.length>1){
	var cpt2=partie[1].length;
	for (var i=0; i<cpt2;i++) {
		$('.chiffre input:eq(' + (1+i+courant) + ')').val(partie[1].charAt(i));}
	}
}
// ##### Effacement du Glisse Nombre 
function vide_bande(){$(".chiffre input").each(function(){$(this).val("");});}
// ##### CORRECTION DU BUG js POUR LES DÉCIMAUX
function decimal(x,y){
	// Transforme le nb X en nb décimal avec une précision de Y chiffres après la virgule
	var reponse = Math.round(x*Math.pow(10,y))/Math.pow(10,y);
	return reponse;
}	
// ##### Ajout des fonctions aux boutons
//---- Ajout de la fonction d'incrémentation à tous les boutons + 
function ajoute_fonction_plus(bouton,no){
	bouton.addEventListener('click', function(){
		increment(no);
		reconstitue_nombre();
		place_nombre(nombre); // Actualise les chiffres du glisse nombre (ajout des 0 manquants)
		},false)
}
//---- Ajout de la fonction de décrémentation à tous les boutons -     
function ajoute_fonction_moins(bouton,no){
	bouton.addEventListener('click', function(){
		decrement(no);
		reconstitue_nombre();
		place_nombre(nombre); // Actualise les chiffres du glisse nombre (ajout des 0 manquants)
		}, false)
}
$("#multiplier .bouton2").click(function(){
	var idx=$(this).html().split('0').length-1;/* Détection de la puissance de 10 */
	glisse(idx);
});
$("#diviser .bouton2").click(function(){
	var idx=$(this).html().split('0').length-1;/* Détection de la puissance de 10 */
	glisse(-idx);
});
// ##### Boutons du menu 
// Retour à la position de départ du Glisse Nombre 
$(".recommencer").click(function(){
	//---- Recharge la page pour revenir en situation intiale.
	location.reload();
});
// Ouverture de la boite de dialogue 
$(".apropos").click(function(){
	$("#infos").dialog("open");
});
//  Mode sombre
$('.sombre').click(function(){
	$('body').toggleClass('mode_sombre');
	});
// ##### Coulissage du Glisse nombre 
//---- Fonction permettant de glisser le nombre vers la gauche ou la droite 
function glisse(idx){
	if (courant+idx>8 && courant+idx<23){ // Limite gauche et droite du glisse nombre
		courant+=idx;// Incrémentation
		nombre=decimal(nombre*Math.pow(10,idx),7); /*Précision à la 7e décimale*/
		//---- Décalle la bande du glisse_nombre 
		$( ".glisse_nombre" ).animate({ "left": "-="+idx*saut+"px" },"slow",function(){
					reconstitue_nombre();place_nombre(nombre)});
	} else {sankore.showMessage(msg_erreur_limite)}
}    

//--------------------------------------------------------------------------------
//----- Fonctions permettant de modifier les chiffres du nombre
//--------------------------------------------------------------------------------
//---- Incrémentation du chiffre de la case     
function increment(no){
	//---- Fonction permettant d'augmenter le chiffre de la case 
	var chiffre=$("input")[no].value;
	switch (chiffre){
	case "9": //---- Si elle contient 9
			if (no>0){ // On modifie sauf si on est au dernier rang
				$("input")[no].value="0"; //---- on revient à 0
				increment(no-1); // On incrémente le chiffre d'avant
			}
			break;
	default: //---- Sinon on ajoute 1 au chiffre de la case
		$("input")[no].value++;
	}
}
//---- Décrémentation du chiffre de la case 	
function decrement(no){
	//---- Fonction permettant de diminuer le chiffre de la case 
	//---- Possibilité de revenir à une case vide 
	var chiffre=$("input")[no].value;
	switch (chiffre){
	case "": case "0": //---- Si le chiffre est 0
		if (!rien_avant(no)) { 
			decrement(no-1); // On décrémenter le chiffre d'avant
			$("input")[no].value=9; //---- on passe à 9
		}
		break;
	default:
		$("input")[no].value--; //---- Sinon on enlève 1 au chiffre de la case
		}
}
function rien_avant(no){
	let i=0,c='',booleen=vrai;
	do {
		c=$('.chiffre input:eq(' + i + ')').val();
		if ((c!="") && (c!="0")){booleen=faux}
		i++;
		} while ((i<=no)) 
	return booleen;
}
function reconstitue_nombre(){
// Reconstitue le nombre à partir des chiffres du glisse nombre
	let i,chaine="";
	for (i=0;i<=courant;i++){
		let c=$('.chiffre input:eq(' + i + ')').val();
		if (c==""){c="0"}
		chaine+=c;
		}
	chaine+=".";
	for (i=courant+1;i<nb_chiffres;i++){
		let c=$('.chiffre input:eq(' + i + ')').val();
		if (c==""){c="0"}
		chaine+=c;
		}
	nombre=parseFloat(chaine); // Conversion de la chaîne de caractères en nombre (suppression des 0 inutiles)
	// Actualisation du nombre
	$("#nombre input").val(nombre.toString().replace(".",separateur)); // Remplacement du point par le séparateur décimal en fonction de la langue utilisée.
	}
//---------------------------------------
//----- Boite de dialogue "A propos"
//---------------------------------------
$("#infos").dialog({ 
	autoOpen: false, 
	show: { effect: 'fade', duration: 500 },
	hide: { effect: 'fade', duration: 250 },
	maxWidth:800,
        maxHeight: 500,
        width: '800',
        height: 'auto',
	position: { my: "left top", at: "right top", of: ".apropos" },
        closeText : "Fermer"
});
